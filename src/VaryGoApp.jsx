import React, {PureComponent} from "react";
import dotProp from "dot-prop";
import Search from "./components/Search/Search";
import PanelDisplay from "./components/Panel/PanelDisplay";
import Panel from "./components/Panel/Panel";
import CoreProvider from "./components/Provider/CoreProvider";
import classNames from "classnames";
import "./css/App.css";

export default class VaryGoApp extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            alert: null,
            panels: [],
        };

        this.handleAlerts = this.handleAlerts.bind(this);
        this.handleAlertClose = this.handleAlertClose.bind(this);
        this.handlePanelUpdate = this.handlePanelUpdate.bind(this);
        this.setSelected = this.setSelected.bind(this);
    }

    componentDidMount() {
        const listeners = dotProp.get(this.props, "listeners");

        // for (const listener in listeners) {
        //     this.props.eventManager.on(listener, listeners[listener]);
        // }

        // We will overwrite the default alert so it doesn't mess with the search, plus it's nicer that way
        window.alert = (alert) => {
            const {alert: prevAlert} = this.state;

            this.props.eventManager.trigger("alert", {
                alert,
                prevAlert,
                handler: this.handleAlerts
            });
        };
    }

    setSelected(selected) {
       console.log(selected);
    }

    handleAlerts(alert) {
        this.setState(state => ({...state, alert}));
    }

    handlePanelUpdate(panels) {
        this.setState(state => ({...state, panels}));
    }

    handleAlertClose() {
        this.props.eventManager.trigger("alertClose", this.handleAlerts);
    }

    buildPanel(panelConfig, index, sections) {
        return <Panel key={index}
                      panelConfig={panelConfig}
                      sections={sections}/>;
    }

    buildAlert() {
        return <div className={classNames("alert", "coloredBackground", {hasAlert: this.state.alert})}>
            <div className="closeButton" onClick={this.handleAlertClose}>X</div>
            {this.state.alert}
        </div>;
    }

    buildSearch() {
        return <Search setSelected={this.setSelected} datasource={this.props.datasource} eventManager={this.props.eventManager}/>;
    }

    buildPanelDisplay(sections) {
        return <PanelDisplay>
            {this.state.panels.map((panelConfig, index) => this.buildPanel(panelConfig, index, sections))}
        </PanelDisplay>;
    }

    render() {
        const sections = dotProp.get(this.props.config, "sections", null);

        if (!sections) {
            throw new Error("Invalid sections");
        }

        return <div className="App">
            {this.buildAlert()}
            <CoreProvider>
                {this.buildSearch()}
                {this.buildPanelDisplay(sections)}
            </CoreProvider>
        </div>;
    }
}
