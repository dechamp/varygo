import React from "react";
import ReactDOM from "react-dom";
import registerServiceWorker from "./registerServiceWorker";
import VaryGoApp from "./VaryGoApp.jsx";
import User from "./components/User";
import Address from "./components/Address";
import Client from "./components/Client";
import Sandbox from "./components/Sandbox";
import ProvidedService from "./components/ProvidedServices";
import AlertListener from "./components/Listener/Alert/DefaultListener.jsx";
import AlertCloseListener from "./components/Listener/Alert/CloseListener.jsx";
import SearchCommandEnablerListener from "./components/Listener/SearchCommand/SearchCommandEnablerListener.jsx";
import SearchCommandGoToSitesListener from "./components/Listener/SearchCommand/SearchCommandGoToSitesListener.jsx";
import SearchPanelByNameEnablerListener from "./components/Search/SearchPanelByNameEnablerListener.jsx";
import SearchPanelByTagsEnablerListener from "./components/Search/SearchPanelByTagsEnablerListener.jsx";
import EventManager from "./components/EventManager";
import DataSourceItem from "./components/DataSource/DataSourceItem";
import md5 from "md5";

const eventManager = new EventManager();

const panelConfig = {
    panels: [
        {
            name: "Sandbox",
            description: "testing",
            tags: [
                "sandbox"
            ],
            section_includes: [
                "sandbox"
            ]
        },
        {
            name: "client address",
            description: "client address panel",
            tags: [
                "client",
                "address"
            ],
            section_includes: [
                "client",
                "address"
            ]
        },
        {
            name: "user",
            tags: [
                "user"
            ],
            section_includes: [
                "user"
            ]
        },
        {
            name: "address",
            tags: [
                "address"
            ],
            section_includes: [
                "address"
            ]
        },
        {
            name: "funky",
            tags: [
                "funky"
            ],
            section_includes: [
                "funky"
            ]
        },
        {
            name: "client",
            tags: [
                "client"
            ],
            section_includes: [
                "client"
            ]
        },
        {
            name: "provided service",
            tags: [
                "provided",
                "service"
            ],
            section_includes: [
                "providedService"
            ]
        },
        {
            name: "Client Provided Service",
            tags: [
                "client",
                "provided",
                "service"
            ],
            section_includes: [
                "client",
                "providedService"
            ]
        },
        {
            name: "main",
            tags: [
                "test",
                "setup",
                "dummy"
            ],
            section_includes: [
                "test",
                "dummy"
            ]
        },
        {
            name: "reverse",
            tags: [
                "reverse",
                "test"
            ],
            section_includes: [
                "dummy",
                "test"
            ]
        },
        {
            name: "userAddress",
            tags: [
                "user address",
                "userAddress"
            ],
            section_includes: [
                "user",
                "address"
            ]
        }
    ]
};

const sectionConfig = {
    sections: {
        sandbox: {
            component: Sandbox
        },
        test: "test section",
        dummy: "dummy section",
        client: {
            component: Client
        },
        providedService: {
            component: ProvidedService
        },
        user: {
            component: User,
            props: [],
            state: ["username"]
        },
        address: {
            component: Address,
            props: [],
            state: ["username"]
        },
        funky: function () {
            return "hi here";
        }
    }
};

const config = {
    ...panelConfig,
    ...sectionConfig
};

const listeners = {
    search: [
        [new SearchCommandEnablerListener(eventManager), "call"],
        [new SearchPanelByNameEnablerListener(panelConfig.panels), "call"],
        [new SearchPanelByTagsEnablerListener(panelConfig.panels), "call"],
    ],
    searchCommand: [
        [new SearchCommandGoToSitesListener(), "call"],
    ],
    alert: [
        [new AlertListener(), "call"]
    ],
    alertClose: [
        [new AlertCloseListener(), "call"]
    ]
};

const filterDataSourceItems = [
    {
        type: "action",
        name: "google",
        description: "launch instance of google.com",
        callback: (searchTerm) => (window.open("https://google.com", "_blank"))
    },
];

function convertPanelConfigToDataSourceItem({name, description, ...rest}) {
    return new DataSourceItem({type: "filter", name, description, meta: {...rest}});
}

const sectionDataSourceItems = panelConfig.panels.map(convertPanelConfigToDataSourceItem);

const allDataSourceItems = [...filterDataSourceItems, ...sectionDataSourceItems];

let datasource = allDataSourceItems.map((props) => new DataSourceItem({...props}));
datasource = datasource.map(source => {
    let newSource = {...source};
    newSource.id = md5(`${source.name}${source.type}${source.description}`);

    return newSource;
});

ReactDOM.render(
    <VaryGoApp datasource={datasource}
               eventManager={eventManager}
               listeners={listeners}
               config={config}/>,
    document.getElementById("root")
);
registerServiceWorker();