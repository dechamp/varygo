import React, {Component} from "react";
import MousePosition from "./MousePosition.jsx";

export default class Sandbox extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div>
            <MousePosition>
                {
                    ({handleMouseMove, position}) =>
                        <div onMouseMove={handleMouseMove} style={{
                            width: 200,
                            height: 200,
                            backgroundColor: "#ccc",
                            color: "#000"
                        }}>Position: {position}</div>
                }
            </MousePosition>
        </div>;
    }
}
