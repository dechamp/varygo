import React, {Component} from "react";
import dotProp from "dot-prop";

export default class Address extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: "",
            address: ""
        };

        this.inputHandler = this.inputHandler.bind(this);
        this.save = this.save.bind(this);
    }

    save(event) {
        event.preventDefault();
        alert(`Saved the client, with address of ${this.state.address}`);
    }

    inputHandler(event) {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });

        if (this.props.hasOwnProperty("isGrouped")) {
            this.props.sharedStateUpdater("client", {
                [name]: value
            });
        }
    }

    render() {
        const client = dotProp.get(this.props, "sharedState.client");

        const formClass = this.props.isGrouped ? "" : "panel-form";
        return (
            <form className={formClass}>
                <div>{client && client.name}</div>
                <label>
                    Address
                    <input type="text"
                           id="address"
                           name="address"
                           onChange={this.inputHandler}
                           value={this.state.address}/>
                </label>
            </form>
        );
    }
}
