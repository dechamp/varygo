export default class EventManager {
    constructor() {
        this.listenerHandles = [];
    }

    on(event, listeners) {
        if (Array.isArray(listeners)) {
            return listeners.map(listener => this.addListener(event, listener));
        }

        return this.addListener(event, listeners);
    }

    off(currentListener) {
        this.listenerHandles = this.listenerHandles.filter(listener => listener !== currentListener);
    }

    addListener(event, listener) {
        const listenerHandle = {};
        listenerHandle.name = event;
        listenerHandle.listener = listener;

        if (this.listenerHandles[event] && this.listenerHandles[event].indexOf(listenerHandle) !== -1) {
            return listenerHandle;
        }

        this.listenerHandles[event] = this.listenerHandles[event] || [];
        this.listenerHandles[event].push(listenerHandle);

        return listenerHandle;
    }

    trigger(event, data = null, allowDisruption = false) {
        const listenerHandles = this.listenerHandles[event] || [];

        if (allowDisruption) {
            listenerHandles.every(handle => this.emit(handle, data));
        } else {
            listenerHandles.forEach(handle => this.emit(handle, data));
        }
    }

    emit(handle, data) {
        const {listener} = handle;

        if (typeof listener === "function") {
            return listener(data);
        } else if (Array.isArray(listener) && listener.length === 2) {
            return listener[0][listener[1]](data);
        }

        throw new Error("Invalid listener");
    }
}