import React, {Component} from "react";
import dotProp from "dot-prop";

export default class ProvidedService extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: "",
            name: ""
        };

        this.inputHandler = this.inputHandler.bind(this);
        this.save = this.save.bind(this);
    }

    save(event) {
        event.preventDefault();
        alert(`Saved the client, with name of ${this.state.name}`);
    }

    inputHandler(event) {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });

        if (this.props.hasOwnProperty("isGrouped")) {
            this.props.sharedStateUpdater("providedService", {
                [name]: value
            });
        }
    }

    render() {
        const formClass = this.props.isGrouped ? "" : "panel-form";
        const client = dotProp.get(this.props, "sharedState.client");
        let clients = [];

        if (client) {
            clients.push(client);
        }

        return (
            <form className={formClass}>
                <label>
                    Client
                </label>
                <select name="client">
                    {clients.map(({clientId = 0, name: clientName = null}, index) => <option key={index}
                                                                                             value={clientId}>{clientName}</option>)}
                </select>
                <input type="text"
                       id="service"
                       name="service"
                       onChange={this.inputHandler}
                       value={this.state.service}/>
                {
                    !this.props.isGrouped &&
                    <button type="submit" onClick={this.save}>
                        Save
                    </button>
                }
            </form>
        );
    }
}
