export default class DataSourceItem {
    constructor({type, name, description, meta = {}, callback = null}) {
        if (/^(?!action|filter)$/.test(type)) {
            throw new Error("Invalid type, please use action or filter");
        }

        this.type = type;
        this.name = name;
        this.description = description;
        this.meta = meta;
        this.callback = callback;
    }
}