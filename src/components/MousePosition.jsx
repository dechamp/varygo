import React, {Component} from "react";

export default class MousePosition extends Component {
    constructor(props) {
        super(props);

        this.state = {
            position: "0:0"
        };

        this.handleMouseMove = this.handleMouseMove.bind(this);
    }

    handleMouseMove(event) {
        const {clientY, clientX} = event;
        this.setState(state => ({...state, position: `${clientY}:${clientX}`}));
    }

    render() {
        const {children} = this.props;
        return <div>{children({handleMouseMove: this.handleMouseMove, position: this.state.position})}</div>;
    }
}
