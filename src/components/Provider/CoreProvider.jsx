import React, {PureComponent} from "react";

export const CoreContext = React.createContext();

export default class CoreProvider extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            name: "peter"
        };

        this.updateHandler = this.updateHandler.bind(this);
    }

    updateHandler(name) {
        this.setState((state) => ({...state, name}));
    }

    render() {
        return (
            <CoreContext.Provider value={{state: this.state, updateHandler: this.updateHandler}}>
                {this.props.children}
            </CoreContext.Provider>
        );
    }
}