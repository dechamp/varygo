import React, {Component} from "react";

export default class Client extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: "",
            name: ""
        };

        this.inputHandler = this.inputHandler.bind(this);
        this.save = this.save.bind(this);
    }

    save(event) {
        event.preventDefault();
        alert(`Saved the client, with name of ${this.state.name}`);
    }

    inputHandler(event) {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });

        if (this.props.hasOwnProperty("isGrouped")) {
            this.props.sharedStateUpdater("client", {
                [name]: value
            });
        }
    }

    render() {
        const formClass = this.props.isGrouped ? "" : "panel-form";

        return (
            <form className={formClass}>
                <label>
                    Client Name
                    <input type="text"
                           id="name"
                           name="name"
                           onChange={this.inputHandler}
                           value={this.state.name}/>
                </label>
                {
                    !this.props.isGrouped &&
                    <button type="submit" onClick={this.save}>
                        Save
                    </button>
                }
            </form>
        );
    }
}
