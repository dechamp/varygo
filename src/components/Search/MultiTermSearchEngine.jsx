export default class MultiTermSearchEngine {
    constructor(datasource, logicCallback) {
        this.datasource = datasource;
        this.logicCallback = logicCallback;
    }

    search(searchTerm) {
        if (!searchTerm) {
            return [];
        }

        const terms = searchTerm.split(",");

        return [].concat(...terms.filter(item => item.length).map(term => this.filterDataSourceByTerm(term.trim())));
    }

    filterDataSourceByTerm(term) {
        return this.datasource.filter(item => this.logicCallback(item, term));
    }
}
