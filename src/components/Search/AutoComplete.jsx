import React, {PureComponent} from "react";
import AutoCompleteItem from "./AutoCompleteItem";
import classNames from "classnames";

export default class AutoComplete extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            selected: [],
            items: {
                idFocused: null
            }
        };

        this.isItemFocused = this.isItemFocused.bind(this);
        this.setItemFocused = this.setItemFocused.bind(this);
        this.isItemSelected = this.isItemSelected.bind(this);
        this.setItemSelected = this.setItemSelected.bind(this);
        this.handleKeyboardNavigation = this.handleKeyboardNavigation.bind(this);
        this.moveToPreviousItem = this.moveToPreviousItem.bind(this);
    }

    isItemFocused(id) {
        if (!this.state.items.idFocused) {
            const items = {...this.state.items};
            items.idFocused = this.props.options[0].id;

            return this.setState(state => ({...state, items}));
        }

        return this.props.focused && this.state.items.idFocused === id;
    }

    setItemFocused(idFocused) {
        const items = {...this.state.items, idFocused};

        this.setState(state => ({...state, items}));
    }

    setItemSelected(id) {
        let {selected: prevSelected} = this.state;

        const selected = prevSelected.includes(id) ?
            [...prevSelected.filter(item => item !== id)] :
            [id, ...prevSelected];

        this.setState(state => ({...state, selected}));
        this.setItemFocused(id);
    }

    isItemSelected(id) {
        return this.state.selected.includes(id);
    }

    handleKeyboardNavigation(event) {
        this.props.focused &&
        this.loadKeyboardEvents(event);
    }

    loadKeyboardEvents(event) {
        const {key} = event;

        switch (key) {
            case "ArrowDown":
                this.handleAutoCompleteDownTrigger();
                break;
            case "ArrowUp":
                this.handleAutoCompleteUpTrigger();
                break;
            case " ":
                this.setFocusedItemAsSelected(event);
                break;
            case "Escape":
                this.handleAutoCompleteEscTrigger(event);
                break;
            case "Enter":
                this.handleAutoCompleteEnterTrigger(event);
                break;
        }
    }

    handleAutoCompleteDownTrigger() {
        this.moveToNextItem();
    }

    handleAutoCompleteUpTrigger() {
        this.moveToPreviousItem();
    }

    handleAutoCompleteEscTrigger() {
        this.props.setFocused(false);
    }

    handleAutoCompleteEnterTrigger() {
        const options = this.props.options.filter(item => this.state.selected.includes(item.id));
        this.props.handleSelected(options);
        this.setItemFocused(0);
        this.clearItemsSelected();
    }

    clearItemsSelected() {
        this.setState(state => ({...state, selected: []}));
    }

    setFocusedItemAsSelected(event) {
        event.preventDefault();

        const {idFocused} = this.state.items;

        if (idFocused) {
            this.setItemSelected(idFocused);
        }
    }

    moveToNextItem() {
        const {options} = this.props;
        const currentFocusedItemId = this.state.items.idFocused;
        const listKey = this.props.options.map(i => i.id).indexOf(currentFocusedItemId);

        if (listKey < 0) {
            return this.setItemFocused(options[0].id);
        }

        const maxId = options.length - 1;
        let newId = listKey + 1;
        newId = newId > -1 ? newId : 0;
        const option = options[newId];

        if (newId <= maxId) {
            this.setItemFocused(option.id);
        }
    }

    moveToPreviousItem() {
        const {options} = this.props;
        const currentFocusedItemId = this.state.items.idFocused;
        const listKey = this.props.options.map(i => i.id).indexOf(currentFocusedItemId);
        let newId = listKey - 1;

        if (newId in options) {
            const option = options[newId];

            return this.setItemFocused(option.id);
        }

        this.setItemFocused(null);
        this.props.setFocused(false);
    }

    componentWillMount() {
        window.document.addEventListener(
            "keydown",
            this.handleKeyboardNavigation,
            false
        );
    }

    componentWillUnmount() {
        window.document.removeEventListener(
            "keydown",
            this.handleKeyboardNavigation,
            false
        );
    }

    buildAutoCompleteItem(option) {
        return <AutoCompleteItem key={option.id}
                                 option={option}
                                 setSelected={this.setItemSelected}
                                 focused={this.isItemFocused(option.id)}
                                 selected={this.isItemSelected(option.id)}/>;
    }

    render() {
        const {focused, options} = this.props;
        const classes = classNames("coloredBackgroundDarkest", {focused});

        return <div id="searchAutoComplete"
                    className={classes}
                    style={{display: options.length ? "block" : "none"}}>
            {options.map((option) => this.buildAutoCompleteItem(option))}
        </div>;
    }
};
