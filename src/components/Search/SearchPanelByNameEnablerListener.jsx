export default class SearchPanelByNameEnablerListener {
    constructor(panels) {
        this.panels = panels;
    }

    call({searchTerm, handlePanelUpdate}) {
        if (/^[A-Z]/.test(searchTerm)) {
            const panels = this.processSearch(searchTerm);
            handlePanelUpdate(panels);
            return false;
        }

        return true;
    }

    processSearch(searchTerm) {
        const terms = searchTerm.split(",");

        return [].concat(...terms.map((term) => this.filterPanelsByTerm(term.trim())));
    }

    filterPanelsByTerm(term = "") {
        return this.panels.filter(panel => panel.name.toUpperCase() === term.toUpperCase());
    }
}
