import React, {PureComponent} from "react";
import Input from "./Input";
import Info from "./Info";
import AutoComplete from "./AutoComplete";
import MultiTermSearchEngine from "./MultiTermSearchEngine";

export default class Search extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            focused: false,
            ref: null,
            info: {
                type: null,
                description: null
            },
            inputTerm: null,
            autoComplete: {
                options: [],
                selected: [],
                focused: false
            }
        };

        function fuzzySearchLogic(datasource, term) {
            const regexTest = `^${term}`;
            const regex = new RegExp(regexTest, "gi");

            return regex.test(datasource.name);
        }

        const actionFuzzyMultiTermSearchLogic = (item, term) => fuzzySearchLogic(item, term) && item.type.toLowerCase() === "action";
        const filterFuzzyMultiTermSearchLogic = (item, term) => fuzzySearchLogic(item, term) && item.type.toLowerCase() === "filter";

        this.actionFuzzyMultiTermSearchEngine = new MultiTermSearchEngine(props.datasource, actionFuzzyMultiTermSearchLogic);
        this.filterFuzzyMultiTermSearchEngine = new MultiTermSearchEngine(props.datasource, filterFuzzyMultiTermSearchLogic);

        this.handleInput = this.handleInput.bind(this);
        this.setAutoCompleteSelected = this.setAutoCompleteSelected.bind(this);
        this.setSearchRef = this.setSearchRef.bind(this);
        this.setSearchFocused = this.setSearchFocused.bind(this);
        this.setAutoCompleteFocused = this.setAutoCompleteFocused.bind(this);
        this.handleSearchInputEvents = this.handleSearchInputEvents.bind(this);
    }

    clearSearch() {
        const {ref} = this.state;

        if (!ref) {
            return;
        }

        ref.value = null;
        ref.blur();
        this.setState(state => ({...state, ref}));
        this.setAutoCompleteInput([]);
        this.setInfo(null, null);
        this.setAutoCompleteFocused(false);
    }

    setSearchRef(ref) {
        if (ref && ref !== this.state.ref) {
            this.setState(state => ({...state, ref}));
        }
    }

    setSearchFocused(focused) {
        if (!typeof(focused) === "boolean") {
            return 0;
        }

        if (focused === false) {
            this.handleSearchInputBlurEvents();
        } else {
            this.handleSearchInputFocusEvents();
        }

        this.setState(state => ({...state, focused}));
    }

    handleSearchInputBlurEvents() {
        window.document.addEventListener(
            "keydown",
            this.handleSearchInputEvents,
            false
        );

        const {ref} = this.state;
        ref && ref.blur();
    }

    handleSearchInputFocusEvents() {
        window.document.addEventListener(
            "keydown",
            this.handleSearchInputEvents,
            false
        );

        const {ref} = this.state;
        ref && ref.focus();
    }

    handleSearchInputEvents(event) {
        const {key} = event;

        switch (key) {
            case "Escape":
                this.handleAutoCompleteEscTrigger(event);
                break;
            case "Enter":
                const {autoComplete} = this.state;
                autoComplete.selected && this.props.setSelected(autoComplete.selected);
                break;
        }
    }

    handleAutoCompleteEscTrigger() {
        this.clearSearch();
    }

    setAutoCompleteFocused(focused) {
        const autoComplete = {...this.state.autoComplete, focused};
        this.setState(state => ({...state, autoComplete}));
        this.setSearchFocused(!focused);
    }

    handleInput({target, key}) {
        if (this.state.focused && this.state.autoComplete.options.length && key === "ArrowDown") {
            this.setSearchFocused(false);
            return this.setAutoCompleteFocused(true);
        }

        let searchTerm = target.value;

        this.runSearchTermPipeline(searchTerm);

        // this.props.eventManager.trigger("search", {
        //     searchTerm,
        //     handlePanelUpdate: this.handlePanelUpdate
        // }, true);
        // handle search
    }

    runSearchTermPipeline(searchTerm) {
        this.getSearchTermPipelines().some(pipeline => pipeline(searchTerm));
    }

    getSearchTermPipelines() {
        return [
            this.actionPipeline.bind(this),
            this.filterPipeline.bind(this),
            this.clearInputPipeline.bind(this)
        ];
    }

    actionPipeline(searchTerm) {
        if (/^:/.test(searchTerm)) {
            this.setInfo("action", "select an action to execute");

            const datasource = this.actionFuzzyMultiTermSearchEngine.search(searchTerm.substr(1));

            this.setAutoCompleteInput(datasource);

            return true; // should stop pipeline
        }

        return false; // no need to stop pipeline
    }

    filterPipeline(searchTerm) {
        this.setInfo("filter", "select all that apply");

        const datasource = this.filterFuzzyMultiTermSearchEngine.search(searchTerm);

        this.setAutoCompleteInput(datasource);

        return false; // no need to stop pipeline
    }

    clearInputPipeline(searchTerm) {
        if (!searchTerm) {
            this.setInfo(null, null);
            this.setAutoCompleteInput([]);
        }

        return true; // no other pipelines should run after this.
    }

    setAutoCompleteInput(datasource) {
        const autoComplete = {...this.state.autoComplete};
        autoComplete.options = datasource;

        this.setState(state => ({...state, autoComplete}));
    }

    setInfo(type, description) {
        const info = {...this.state.info, type, description};

        this.setState(state => ({...state, info}));
    }

    setAutoCompleteSelected(selected) {
        const autoComplete = {...this.state.autoComplete, selected};

        this.setState(state => ({...state, autoComplete}));
        this.setAutoCompleteFocused(false);
    }

    render() {
        const info = {...this.state.info};
        const options = [...this.state.autoComplete.options];

        return (
            <div id="search">
                <div id="searchInsideWrapper">
                    <Input handleInput={this.handleInput}
                           selected={this.state.autoComplete.selected}
                           setFocused={this.setSearchFocused}
                           setReference={this.setSearchRef}/>
                    {info.description && <Info {...info}/>}
                    {this.state.autoComplete.options.length > 0 &&
                    <AutoComplete focused={this.state.autoComplete.focused}
                                  setFocused={this.setAutoCompleteFocused}
                                  listeners={this.props.listeners}
                                  options={options}
                                  handleSelected={this.setAutoCompleteSelected}/>}
                </div>
            </div>
        );
    }
}
