import React from "react";
import classNames from "classnames";

export default function AutoCompleteItem({option: {id, name, description}, setSelected, selected = false, focused = false}) {
    const classes = classNames("searchAutoCompleteItem", "coloredBackgroundDark", {selected, focused});

    return (
        <div onClick={() => setSelected(id)} className={classes}>
            <strong>{id}:{name}</strong>{description && `: ${description}`}
        </div>
    );
};
