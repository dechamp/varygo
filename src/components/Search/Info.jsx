import React from "react";

const Info = ({type, description}) => (
    <div id="searchInfo"
         className="coloredBackgroundDarkest">
        <span>[{type}] {description}</span>
    </div>
);

export default Info;
