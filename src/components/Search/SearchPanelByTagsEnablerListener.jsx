export default class SearchPanelByTagsEnablerListener {
    constructor(panels) {
        this.panels = panels;
    }

    search({searchTerm, handlePanelUpdate}) {
        if (/^[^A-Z]/.test(searchTerm)) {
            const panels = this.processSearch(searchTerm);
            handlePanelUpdate(panels);
            return false;
        }

        return true;
    }

    processSearch(searchTerm) {
        const terms = searchTerm.split(",");

        return [].concat(...terms.map((term) => this.filterPanelsByTerm(term.trim())));
    }

    filterPanelsByTerm(term = "") {
        return this.panels.filter(
            panel => panel.tags.some(tag => tag.toLowerCase() === term.toLowerCase())
        );
    }
}
