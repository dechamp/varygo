import React, {PureComponent} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import classNames from "classnames";

export default class Input extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            focused: false,
            inputReference: null
        };

        this.handleReference = this.handleReference.bind(this);
        this.setFocused = this.setFocused.bind(this);
        this.setBlur = this.setBlur.bind(this);
    }

    handleReference(inputReference) {
        this.setState(state => ({...state, focused: true, inputReference}));
        this.props.setReference(inputReference);
    };

    setFocused() {
        this.setState(state => ({...state, focused: true}));
        this.props.setFocused(true);
    }

    setBlur() {
        this.setState(state => ({...state, focused: false}));
        this.props.setFocused(false);
    }

    render() {
        const classes = classNames({focused: this.state.focused}, "coloredBackground");

        return (
            <div id="searchInput" className={classes}>
                <label>
                    <FontAwesomeIcon icon={faSearch}/>
                </label>
                {/*<div>xxx{this.props.selected.map(item => `<div class="block">${item.name}</div>`)}</div>this.state.inputReference.value =*/}
                <input id="search"
                       name="search"
                       className="coloredBackgroundDarkest"
                       onFocus={this.setFocused}
                       onBlur={this.setBlur}
                       type="text"
                       autoComplete='off'
                       onKeyUp={this.props.handleInput}
                       ref={this.handleReference}
                       autoFocus="autofocus"/>
            </div>
        );
    }
};
