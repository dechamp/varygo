export default class DefaultListener {
    call({alert, prevAlert = null, handler = null}) {
        if (prevAlert) {
            return handler([prevAlert, alert].join(", "));
        }

        return handler(alert);
    }
}
