export default class SearchCommandGoToSitesListener {
    call(searchTerm) {
        const sites = {
            google: "https://google.com",
            gmail: "https://gmail.com",
            reddit: "https://reddit.com",
        };

        const url = sites[searchTerm.toLowerCase()];

        if (url) {
            window.open(url, "_blank");

            return false;
        }

        return true;
    }
}
