export default class SearchCommandEnablerListener {
    constructor(eventListener) {
        this.eventListener = eventListener;
    }

    call({searchTerm}) {
        if (/^:/.test(searchTerm)) {
            return this.eventListener.trigger("searchCommand", searchTerm.substring(1));
        }

        return true;
    }
}
