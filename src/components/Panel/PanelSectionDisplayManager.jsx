import React from "react";

export default function PanelSectionDisplayManager({section, isGrouped = null, sharedStateUpdater = null, sharedState = null}) {
    let response = null;

    if (section && typeof section === "string") {
        response = <div dangerouslySetInnerHTML={{__html: section}}/>;
    } else if (section && typeof section === "function") {
        response = section();
    } else if (section && typeof section === "object" && section.hasOwnProperty("component")) {
        if (isGrouped) {
            response = <section.component sharedStateUpdater={sharedStateUpdater}
                                          sharedState={sharedState}
                                          isGrouped={isGrouped}/>;
        } else {
            response = <section.component/>;
        }
    } else if (section && typeof section === "object" && React.isValidElement(section)) {
        response = section;
    } else {
        throw new Error("You have a mis-configured section");
    }

    return response;
}
