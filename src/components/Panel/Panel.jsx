import React from "react";
import dotProp from "dot-prop";
import PanelSectionDisplayManager from "./PanelSectionDisplayManager";
import SectionErrorBoundary from "../Error/SectionErrorBoundary";
import ContextPanelSectionGroupManager from "./ContextPanelSectionGroupManager";


export default function Panel(props) {
    const panelConfig = dotProp.get(props, "panelConfig", null);
    const sections = dotProp.get(props, "sections", null);

    guardSection(sections);
    guardPanelConfig(panelConfig);

    const name = dotProp.get(panelConfig, "name", "");
    const panelSections = getPanelSections(panelConfig, sections);

    let response = "No panel sections found";

    if (panelSections.length > 1) {
        response = <ContextPanelSectionGroupManager title={name}>
            {panelSections}
        </ContextPanelSectionGroupManager>;

    } else if (panelSections.length === 1) {
        response = <PanelSectionDisplayManager section={panelSections[0]}/>;
    }

    return <div className="panel">
        <SectionErrorBoundary>{response}</SectionErrorBoundary>
    </div>;
}

function guardSection(sections) {
    if (!sections) {
        throw new Error("Invalid sections");
    }
}

function guardPanelConfig(panelConfig) {
    if (!panelConfig || !panelConfig.name || !panelConfig.section_includes) {
        throw new Error("Invalid panelConfig object");
    }
}

function getPanelSections(panelConfig, sections) {
    return panelConfig.section_includes.map((sectionInclude) => {
        return dotProp.get(sections, sectionInclude, null);
    });
}
