import React, {PureComponent} from "react";
import PanelSectionDisplayManager from "./PanelSectionDisplayManager";

export default class ContextPanelSectionGroupManager extends PureComponent {
    updateSectionSharedState = (sectionKey, value) => {
        if (this.state.sectionSharedState[sectionKey] !== value) {
            this.setState(state =>
                ({...state, sectionSharedState: {...state.sectionSharedState, [sectionKey]: value}}));
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            children: this.props.children,
            sectionSharedState: {}
        };
    }

    render() {
        const formComponents = this.props.children.map((section, index) => {
            return <PanelSectionDisplayManager key={index}
                                               section={section}
                                               sharedStateUpdater={this.updateSectionSharedState}
                                               sharedState={this.state.sectionSharedState}
                                               isGrouped={true}/>;
        });

        return (formComponents);
    }
}
