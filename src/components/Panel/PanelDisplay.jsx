import React from "react";
import dotProp from "dot-prop";

export default function PanelDisplay(props) {
    const panels = dotProp.get(props, "children", []);

    let response = <div className="no-results">no results</div>;

    if (panels.length) {
        response = panels;
    }

    return <div id="panelDisplay" className={props.isSearchActive ? "search" : ""}>{response}</div>;
}
