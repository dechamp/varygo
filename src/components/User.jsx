import React, {Component} from "react";

export default class User extends Component {
    save = () => {
        console.log("Saving User");
    };
    handleChange = (event) => {
        if (this.props.updater) {
            this.props.updater(event.target.id, event.target.value);
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            username: props.username
        };
    }

    render() {
        const formClass = this.props.isGrouped ? "" : "panel-form";
        return (
            <form className={formClass}>
                <label>
                    User name:
                    <input type="text" id="username" value={this.state.username} onChange={this.handleChange}/>
                </label>
                {!this.props.isGrouped &&
                <button type="submit" onClick={this.save}>
                    Save
                </button>
                }
            </form>
        );
    }
}
