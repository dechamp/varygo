import React, {PureComponent} from "react";

export default class ErrorBoundary extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            hasError: false
        };
    }

    componentDidCatch(error, info) {
        this.setState((state) => ({...state, hasError: true}));
        console.log(error, info);
    }

    render() {
        if (this.state.hasError) {
            return <h1>Unknown issue happened</h1>;
        }

        return <div>{this.props.hasOwnProperty("children") && this.props.children}</div>;
    }
}
