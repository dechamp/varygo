import React, {PureComponent} from "react";

export default class SectionErrorBoundary extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            hasError: false
        };
    }

    componentDidCatch(error, info) {
        this.setState((state) => ({...state, hasError: true}));
    }

    render() {
        if (this.state.hasError) {
            return <h2>Section failed to load</h2>;
        }

        return <div>{this.props.hasOwnProperty("children") && this.props.children}</div>;
    }
}
